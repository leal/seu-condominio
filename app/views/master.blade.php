<!doctype html>
<html lang="en">
<head>
  <title>
  @section('title')
    Seu Condominio
  @show
  </title>
  <link rel="shortcut icon" href="{{{asset("")}}}">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  {{ HTML::style('css/bootstrap.min.css') }}
  {{-- HTML::style('css/fa/css/font-awesome.min.css') --}}
  {{ HTML::style('css/master.css') }}

  @section('css')
  @show

  <!-- Scripts are placed here -->
  {{ HTML::script('js/jquery-2.1.3.min.js') }}
  {{ HTML::script('js/bootstrap.min.js') }}
  {{ HTML::script('js/script.js') }}

  @section('js')
  @show

</head>
<body>
  <nav class="navbar menu-nav navbar-fixed-top" role="navigation">
    <div class="container-fluid menu-container">
      <div class="navbar-left">
        <img src="img/logo.png" class="pull-left" alt="Seu condomínio" height="40" width="40">
      </div>
      <div class="navbar-left">
        <a class="pull-left" style="font-size: 15px;" href="{{ URL::to('') }}">
          <b>Seu <br>condomínio</b>
        </a>
      </div>
      <div class="navbar-right">
        <a href="#sair" class="pull-right">Sair</a>
      </div>
    </div>
  </nav>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 pull-right lista">
        <a id="cadastrar" href="#new" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i> Cadastrar Tarefa </a>
      </div>
    </div>
    <br/>
    <div class="row">
      <ul class="nav nav-pills nav-stacked cadastrar" hidden>
          <li class="active col-lg-12">
            <a href="#"><i class="glyphicon glyphicon-chevron-down"></i> Tarefa</a>
            <div>
              <form class="form-horizontal detalhes" id="nova-tarefa">
                <fieldset>
                  <input name="id" type="hidden">

                <div class="control-group">
                  <label class="control-label" for="textinput">Nome da tarefa:</label>
                  <div class="input-group-sm">
                    <input name="nome" class="form-control" type="text">
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label" for="appendedtext">Início:</label>
                  <div class="controls">
                    <div class="input-group input-group-sm">
                      <input name="inicio" class="form-control" placeholder="__/__/___" type="text">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                  </div>
                </div>

                <div class="control-group">
                  <label class="control-label" for="appendedtext">Conclusão:</label>
                  <div class="controls">
                    <div class="input-group input-group-sm">
                      <input name="conclusao" class="form-control" placeholder="__/__/___" type="text">
                      <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                  </div>
                </div>


                <!-- Prepended text-->
                <div class="control-group">
                  <label class="control-label" for="prependedtext">Custo estimado (R$):</label>
                  <div class="controls">
                    <div class="input-group input-group-sm">
                      <span class="input-group-addon">R$</span>
                      <input name="custo" class="form-control" type="text">
                    </div>
                  </div>
                </div>

                <!-- Select Basic -->
                <div class="control-group">
                  <label class="control-label" for="selectbasic">Status da tarefa:</label>
                  <div class="controls select-sm">
                    <select name="status" class="form-control">
                      <option value="C">Concluído</option>
                      <option value="P">Pendente</option>
                      <option value="E">Em andamento</option>
                    </select>
                  </div>
                </div>
                <br/>
                <!-- Button (Double) -->
                <div class="control-group">
                  <div class="controls pull-right">
                    <button id="voltar" name="button2id" class="btn btn-primary">Voltar</button>
                    <button id="apagar" name="button2idd" class="btn btn-danger">Apagar</button>
                    <button id="button1id" name="button1id" class="btn btn-success">Salvar</button>
                  </div>
                </div>

                </fieldset>
                </form>

            </div>
          </li>
        </ul>



        <ul id="lista-tarefas" class="nav nav-pills nav-stacked lista">
          <li class="active col-lg-12">
            <a href="#" class="expandir"><i class="glyphicon glyphicon-chevron-right"></i> <b>Tarefa 1</b> <i class="glyphicon glyphicon-pencil pull-right edit-item"></i></a>
            <div class="detalhes text-center" hidden>
              <table class="table table-date">
                <thead>
                  <tr>
                      <td>Início</td>
                      <td>Conclusão</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                      <td>12/12/1212</td>
                      <td>21/12/1212</td>
                  </tr>
                </tbody>
            </table>
              <br/>
            <table class="table">
                <thead>
                  <tr>
                      <td>Custo Estimado</td>
                      <td>Status da tarefa</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                      <td>R$ 10.000,00</td>
                      <td>Em andamento</td>
                  </tr>
                </tbody>
            </table>
            </div>
          </li>

          <li class="active col-lg-12">
            <a href="#" class="expandir"><i class="glyphicon glyphicon-chevron-right"></i> Nova Tarefa 2</a>
            <div class="detalhes" hidden>
              lkakÇLKÇ
            </div>
          </li>
        </ul>
    </div>



<!--    <div class="row">
      <div class="col-md-2 sidebar">
          <div class="row">
            <div class="col-md-12">
              <div class="media-user">
                <img src="/">
                <span class="">Usuário</span>
              </div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-12">
              <ul class="list-unstyled menu-left-list">
                <li><span class=""><i class="glyphicon glyphicon-bookmark"></i>  Meus Lotes</span></li>
                <li><span class=""><i class="glyphicon glyphicon-grain"></i>  Alimentos</span></li>
                <li><span class=""><i class="glyphicon glyphicon-piggy-bank"></i>  Animais</span></li>
                <li><span class=""><i class="glyphicon glyphicon-usd"></i>  Créditos</span></li>
                <li><span class=""><i class=""></i>Ajuda</span></li>
              </ul>
            </div>
          </div>
      </div>
      <div class="col-md-9 col-md-offset-2 main">
        <div class="row">
          <div class="col-md-12">
            <button class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Novo Lote</button>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-3 col-sm-4">
            <div class="panel panel-default">
              <div class="panel-heading">
                LOTE
              </div>
              <div class="panel-body">
                <br>
                <br>
                <br>
                <br>
                <button class="btn btn-primary btn-block">Calcular Ração</button>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-4">
            <div class="panel panel-default">
              <div class="panel-heading">
                LOTE
              </div>
              <div class="panel-body">
                <br>
                <br>
                <br>
                <br>
                <button class="btn btn-primary btn-block">Calcular Ração</button>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-4">
            <div class="panel panel-default">
              <div class="panel-heading">
                LOTE
              </div>
              <div class="panel-body">
                <br>
                <br>
                <br>
                <br>
                <button class="btn btn-primary btn-block">Calcular Ração</button>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-4">
            <div class="panel panel-default">
              <div class="panel-heading">
                LOTE
              </div>
              <div class="panel-body">
                <br>
                <br>
                <br>
                <br>
                <button class="btn btn-primary btn-block">Calcular Ração</button>
              </div>
            </div>
          </div>
        </div>
-->

      </div>
    </div>
  </div>



</body>
</html>
