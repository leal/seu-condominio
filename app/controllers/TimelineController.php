<?php

class TimelineController extends \BaseController {

	public function getIndex()
	{
		return View::make('master');
	}

  public function anyGetTarefas()
  {
    $tarefas = Tarefa::all();
    foreach($tarefas as $tarefa)
      $tarefa->id = Crypt::encrypt($tarefa->id);

    return $tarefas;
  }

  public function anyGetComentarios($tarefa)
  {
    $comentarios = Comentario::whereTarefa(Crypt::decrypt($tarefa));
    foreach($comentarios as $comentario)
      $comentario->id = Crypt::encrypt($comentario->id);

    return $comentarios;
  }

  public function anyInsertTarefa()
  {
    $tarefa = null;
    try
    {
      if( !strlen(Input::get("id")) )
      {
        $tarefa = new Tarefa;

      }
      else
      {
        $tarefa = Tarefa::find(Crypt::decrypt(Input::get("id")));
      }

      $tarefa->nome = Input::get("nome");
      $tarefa->inicio = Input::get("inicio");
      $tarefa->conclusao = Input::get("conclusao");
      $tarefa->custo = Input::get("custo");
      $tarefa->status = Input::get("status");
      $tarefa->save();

      return Response::JSON(true);
    }
    catch (Exception $e)
    {
      return Response::JSON(false);
    }
  }

  public function anyDeleteTarefa()
  {
    $tarefa = Tarefa::find(Crypt::decrypt(Input::get("id")));
    $tarefa->delete();
  }

}