var statusTarefa = {C:"Concluído", E:"Em andamento", P:"Pendente"};

function update()
{
  $.post("/get-tarefas", {}, function(data){
    console.log(JSON.parse(JSON.stringify(statusTarefa)));
    var html = "";

    for(var i = 0 ; i < data.length ; i++ )
    {
      html += '<li class="active col-lg-12">'
              +'<a href="#" class="expandir"><i class="glyphicon glyphicon-chevron-right alternate"></i> <b>'+data[i].nome+'</b>'
                  +'<i class="glyphicon glyphicon-pencil pull-right edit-item" data=\''+JSON.stringify(data[i])+'\'></i></a>'
              +'<div class="detalhes text-center" hidden>'
                +'<table class="table table-date">'
                  +'<thead>'
                    +'<tr>'
                        +'<td>Início</td>'
                        +'<td>Conclusão</td>'
                    +'</tr>'
                  +'</thead>'
                  +'<tbody>'
                    +'<tr>'
                        +'<td>'+data[i].inicio+'</td>'
                        +'<td>'+data[i].conclusao+'</td>'
                    +'</tr>'
                  +'</tbody>'
              +'</table>'
                +'<br/>'
              +'<table class="table">'
                  +'<thead>'
                    +'<tr>'
                        +'<td>Custo Estimado</td>'
                        +'<td>Status da tarefa</td>'
                    +'</tr>'
                  +'</thead>'
                  +'<tbody>'
                    +'<tr>'
                        +'<td>R$ '+data[i].custo+'</td>'
                        +'<td>'+statusTarefa[data[i].status]+'</td>'
                    +'</tr>'
                  +'</tbody>'
              +'</table>'
              +'</div>'
            +'</li>';
      }
    $("#lista-tarefas").html(html);

    $(".expandir").click(function(){
      $(this).nextAll(".detalhes").toggle();
      if($(this).find(".alternate").hasClass("glyphicon-chevron-right"))
        $(this).find(".alternate").removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
      else
        $(this).find(".alternate").addClass("glyphicon-chevron-right").removeClass("glyphicon-chevron-down");
      return false;
    });

    $(".edit-item").click(function(){
      $(".cadastrar, #apagar").show();
      $(".lista").hide();
      var data = JSON.parse($(this).attr("data"));
      for( var i = 0 ; i < $("#nova-tarefa input, #nova-tarefa select").length ; i++ )
        $("#nova-tarefa input, #nova-tarefa select").eq(i).val(data[$("#nova-tarefa input, #nova-tarefa select").eq(i).attr("name")]);

      console.log(data);
    });

  });
}

$(function(){
  $("#cadastrar").click(function(){
    $("#nova-tarefa input").val("");
    $(".cadastrar").show();
    $(".lista, #apagar").hide();
  });

  $("#voltar").click(function(){
    $(".cadastrar").hide();
    $(".lista").show();
    return false;
  });

  $("#nova-tarefa").submit(function(){
    var data = {};
    for( var i = 0 ; i < $("#nova-tarefa input, #nova-tarefa select").length ; i++ )
      data[$("#nova-tarefa input, #nova-tarefa select").eq(i).attr("name")] = $("#nova-tarefa input, #nova-tarefa select").eq(i).val();
    console.log( data );

    $.post("/insert-tarefa", data, function(data){
      console.log(data);
      if (!data)
        alert("Operação inválida.");
      else
      {
        update();
        $("#voltar").click();
      }
    });
    return false;
  });

  $("#apagar").click(function(){
    if (confirm("Tem certeza que quer apagar?"))
    {
      $.post("/delete-tarefa", {
        id: $("#nova-tarefa input[name='id']").val()
      }, function(data){
        console.log(data);
        update();
        $("#voltar").click();
      });
    }
    return false;
  });

  update();
});